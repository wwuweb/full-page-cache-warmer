# wwuweb/full-page-cache-warmer

## About

This command-line application provides the means to crawl a standardized
sitemap.xml document with the puppeteer API for the Chrome/Chromium web browser.

This effects full loading of all assets of every page in the sitemap, providing
full-coverage cache-warming or static generation of assets.

Because this tool uses a full web browser under the hood, it presents to the
host (or proxy) as a full web client, ensuring expected response behavior.

As an optimization measure, requests to any host other than the intended target
for crawling are aborted. In addition, uncacheable pages are ignored in order to
  reduce unnecessary network traffic and server load.

## Installation

```
npm install
```

## Usage

```
Usage: -h <host>

Options:
      --help     Show help                                             [boolean]
      --version  Show version number                                   [boolean]
  -h, --host     The host of the website on which to perform a cache warming
                 operation.                                  [string] [required]
```
