'use strict';

const axios = require('axios');
const xml = require('fast-xml-parser');

module.exports.fetch = async endpoint => {
  const { data } = await axios.get(endpoint);
  const sitemap = xml.parse(data);

  return sitemap.urlset.url;
};
