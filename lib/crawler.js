'use strict';

const https = require('https');
const axios = require('axios');
const puppeteer = require('puppeteer');

const options = {
  defaultViewport: {
    width: 1920,
    height: 1080,
    deviceScaleFactor: 1,
    isMobile: false,
    hasTouch: false,
    isLandscape: false
  }
};

function output(requestUrl, response, message) {
  console.log(`${requestUrl.pathname}\n  => (${response.status()}): ${message}`);
}

function hit(headers) {
  return ('x-varnish' in headers) && (headers['x-varnish'].split(' ').length === 2);
}

module.exports.crawl = async (host, entries) => {
  const instance = axios.create({ httpsAgent: new https.Agent({ keepAlive: true }) });
  const browser = await puppeteer.launch(options);
  const page = await browser.newPage();
  const paths = new Set();

  const exclusions = [
    'image/png',
    'image/svg',
  ];

  const retry = async (url, tries = 3) => {
    if (tries < 1) {
      return false;
    }

    const response = await instance.get(url);

    if (hit(response.headers)) {
      return true;
    }

    return await retry(url, tries - 1);
  };

  await page.setCacheEnabled(false);
  await page.setRequestInterception(true);

  page.on('request', request => {
    const requestUrl = new URL(request.url());

    if (request.method() === 'POST') {
      request.abort();
    }
    else if (paths.has(requestUrl.pathname) && request.redirectChain().length === 0) {
      request.abort();
    }
    else if (requestUrl.host === host) {
      paths.add(requestUrl.pathname);
      request.continue();
    }
    else {
      request.abort();
    }
  });

  page.on('response', async response => {
    const requestUrl = new URL(response.request().url());

    if (exclusions.some(exclusion => requestUrl.pathname.includes(exclusion))) {
      return;
    }

    const headers = response.headers();

    if ('x-varnish' in headers) {
      try {
        const cached = hit(headers) || await retry(requestUrl.toString());

        if (cached) {
          output(requestUrl, response, 'HIT');
        }
        else {
          output(requestUrl, response, 'MISS');
        }
      }
      catch (error) {
        console.log('Error:', error.message);
      }
    }
    else {
      output(requestUrl, response, 'Not handled by Varnish.');
    }
  });

  for (let entry of entries) {
    const { loc } = entry;

    try {
      await page.goto(loc, { waitUntil: 'domcontentloaded' });
    }
    catch (error) {
      console.log('Error:', error.message);
    }
  }

  await browser.close();
};
