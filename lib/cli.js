'use strict';

const yargs = require('yargs');
const sitemap = require('./sitemap');
const crawler = require('./crawler');

const options = {
  host: {
    alias: 'host',
    describe: 'The host of the website on which to perform a cache warming operation.',
    type: 'string',
    demandOption: true
  }
}

module.exports = async () => {
  const usage = yargs.usage('Usage: -h <host>');
  const params = usage.option("h", options.host);
  const { argv } = params;
  const { host } = argv;
  const endpoint = 'https://' + host + '/sitemap.xml';

  console.log("\n" + `Reading sitemap of ${host}...` + "\n");

  try {
    const entries = await sitemap.fetch(endpoint);

    console.log('Sitemap loaded succesfully. Initiating cache-warming crawl ...', "\n");

    await crawler.crawl(host, entries);
  }
  catch (error) {
    console.log('Error:', error.message);
  }
};
